﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EjercicioTablas.Startup))]
namespace EjercicioTablas
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
