
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/15/2015 08:14:21
-- Generated from EDMX file: D:\Documentos Yax\Documentos\Visual Studio 2013\Projects\EjercicioTablas\EjercicioTablas\Models\ModeloTablas.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [BDTablas];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_PERSONAUSUARIO]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PERSONASet] DROP CONSTRAINT [FK_PERSONAUSUARIO];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[USUARIOSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[USUARIOSet];
GO
IF OBJECT_ID(N'[dbo].[PERSONASet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PERSONASet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'USUARIOSet'
CREATE TABLE [dbo].[USUARIOSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [User] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PERSONASet'
CREATE TABLE [dbo].[PERSONASet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Apellido] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [Telefono] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'USUARIOSet'
ALTER TABLE [dbo].[USUARIOSet]
ADD CONSTRAINT [PK_USUARIOSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PERSONASet'
ALTER TABLE [dbo].[PERSONASet]
ADD CONSTRAINT [PK_PERSONASet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------